package in.demoapp.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.demoapp.modelsx.Quote;
import in.demoapp.modelsx.Quotes;
import in.demoapp.reposx.QuotesRepo;

@RestController
@RequestMapping("/rest/db")
public class DBServiceController {

	@Autowired
	private QuotesRepo repo;

	@GetMapping("/{username}")
	public List<String> getQuotes(@PathVariable("username") final String username) {
		return getQuotesByUserName(username);
	}

	@PostMapping("/add")
	public List<String> add(@RequestBody final Quotes quotes) {
		quotes.getQuotes().stream().map(quote -> new Quote(quotes.getUsername(), quote))
				.forEach(quote -> repo.save(quote));

		return getQuotesByUserName(quotes.getUsername());
	}

	@PostMapping("/delete/{username}")
	public List<String> delete(@PathVariable("username") final String username) {
		List<Quote> quotes = repo.findByUsername(username);
		repo.delete(quotes);

		return getQuotesByUserName(username);
	}

	private List<String> getQuotesByUserName(@PathVariable("username") String username) {
		return repo.findByUsername(username).stream().map(Quote::getQuote).collect(Collectors.toList());
	}

}
