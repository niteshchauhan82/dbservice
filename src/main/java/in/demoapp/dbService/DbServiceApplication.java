package in.demoapp.dbService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;



@EnableEurekaClient
@EnableJpaRepositories(basePackages="in.demoapp.*")
@EntityScan("in.demoapp.*")
@SpringBootApplication
@ComponentScan(basePackages = "in.demoapp.*")
public class DbServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbServiceApplication.class, args);
	}
}
