package in.demoapp.reposx;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import in.demoapp.modelsx.Quote;

public interface QuotesRepo extends JpaRepository<Quote, Integer> {
	List<Quote> findByUsername(String username);
}
