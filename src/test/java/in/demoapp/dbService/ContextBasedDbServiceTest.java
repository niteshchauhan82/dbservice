package in.demoapp.dbService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import in.demoapp.modelsx.Quote;
import in.demoapp.reposx.QuotesRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ContextBasedDbServiceTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private QuotesRepo repo;

	@Test
	public void testGet() throws Exception {
		List<Quote> quotes = new ArrayList<Quote>();

		Quote quote = new Quote();
		quote.setUsername("nitesh");
		quote.setQuote("GOOG");
		quotes.add(quote);

		given(repo.findByUsername("nitesh")).willReturn(quotes);

		MockHttpServletResponse response = mvc.perform(get("/rest/db/nitesh").accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		
		// other ways you can use it
		//Mockito.when(connection.post(Mockito.anyString(), Mockito.any(Form.class))).thenReturn(someresponse);
		//Mockito.when(responseLogin.readEntity(Mockito.any(Class.class))).thenReturn(someresponse);

		//Mockito.when(connection.post(Mockito.anyString(), Mockito.any(Form.class), Mockito.any(SomeClass.class))).thenReturn(response);
		//Mockito.when(response.readEntity(Mockito.any(Class.class))).thenReturn(RESPONSE);
	}

}
