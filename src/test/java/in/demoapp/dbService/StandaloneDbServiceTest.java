package in.demoapp.dbService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import in.demoapp.controller.DBServiceController;
import in.demoapp.modelsx.Quote;
import in.demoapp.reposx.QuotesRepo;

@RunWith(MockitoJUnitRunner.class)
public class StandaloneDbServiceTest {
	
	private MockMvc mvc;
	
	@Mock
	private QuotesRepo repo;

	@InjectMocks
	private DBServiceController controller;
	
	@Before
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
	}
	
	@Test
	public void testGet() throws Exception {
		System.out.println("Initiating Mock Test - Basic GET");
		List<Quote> quotes = new ArrayList<Quote>();
		
		Quote quote = new Quote();
		quote.setUsername("nitesh");
		quote.setQuote("GOOG");
		quotes.add(quote);
		
		given(repo.findByUsername("nitesh")).willReturn(quotes);
		
		MockHttpServletResponse response = mvc.perform(get("/rest/db/nitesh")//.param("username", "nitesh")
											  .accept(MediaType.APPLICATION_JSON))
											  .andReturn()
											  .getResponse();
		
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		
	}

}
